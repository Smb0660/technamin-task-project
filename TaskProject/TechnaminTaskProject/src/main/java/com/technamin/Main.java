package com.technamin;

import com.technamin.model.Document;
import com.technamin.service.DocumentCollectorService;
import com.technamin.service.ThreadBalancer;
import com.technamin.service.impl.DocumentCollectorServiceImpl;
import com.technamin.service.impl.ThreadBalancerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

public class Main {
    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        try {
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            LOGGER.info(e.getMessage());
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
        DocumentCollectorService documentCollectorService = new DocumentCollectorServiceImpl();
        ThreadBalancer name = new ThreadBalancerImpl();
        Map<Long, List<Document>> documents = documentCollectorService.convertDocumentListToMap();
        name.balanceThreads(List.of(documents.keySet().toArray(new Long[0])), documents);
    }
}