package com.technamin.constants;

public class Messages {
    private Messages(){
    }

    public static class SuccessMessages {
        private SuccessMessages(){
        }
        public static  final String DOCUMENT_SUCCESSFULLY_SAVED = "Document with id:{} is successfully saved";
    }

    public static class ErrorMessages{
        private ErrorMessages(){
        }
        public static final String PATH_EXCEPTION = "Path to file is incorrect";
    }

    public static class InfoMessages{
        private InfoMessages(){
        }
        public static final String UPDATE_RESULT_INFO = "Update result is :{}";
    }
}