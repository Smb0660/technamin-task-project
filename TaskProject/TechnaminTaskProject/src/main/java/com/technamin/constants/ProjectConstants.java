package com.technamin.constants;

public class ProjectConstants {

    private ProjectConstants(){
        throw new IllegalStateException();
    }

    public static final String DOCUMENT_FILE_PATH = "src/main/java/com/technamin/input.json";
    public static final String RABBITMQ_QUEUE_NAME = "documents_queue";
    public static final String RABBITMQ_HOST = "rabbitmq";
    public static final String RABBITMQ_VIRTUAL_HOST = "/";
    public static final int RABBITMQ_PORT = 5672;
    public static final String RABBITMQ_USERNAME = "guest";
    public static final String RABBITMQ_PASSWORD = "guest";
    public static final String MONGO_DB_HOST = "mongodb";
    public static final int MONGO_DB_PORT = 27017;
    public static final String MONGO_DB_DATABASE_NAME = "documents";
    public static final String MONGO_DB_COLLECTION_NAME = "documents";
}