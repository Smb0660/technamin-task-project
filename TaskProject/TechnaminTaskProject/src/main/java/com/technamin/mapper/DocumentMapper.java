package com.technamin.mapper;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.technamin.model.Document;

import java.io.Reader;
import java.util.List;

public class DocumentMapper {

    private DocumentMapper(){
        throw new IllegalStateException();
    }

    public static List<Document> documentJsonParser(Reader reader) {
        return new Gson().fromJson(reader, new TypeToken<List<Document>>() {}.getType());
    }
}