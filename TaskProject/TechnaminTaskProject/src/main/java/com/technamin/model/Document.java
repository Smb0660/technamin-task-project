package com.technamin.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class Document implements Serializable {
    @SerializedName("doc_id")
    private long documentID;
    @SerializedName("seq")
    private long sequence;
    @SerializedName("data")
    private String data;
    @SerializedName("time")
    private long time;

    public long getDocumentID() {
        return documentID;
    }

    public void setDocumentID(long documentID) {
        this.documentID = documentID;
    }

    public long getSequence() {
        return sequence;
    }

    public void setSequence(long sequence) {
        this.sequence = sequence;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Document{" +
                "documentID=" + documentID +
                ", sequence=" + sequence +
                ", data='" + data + '\'' +
                ", time=" + time +
                '}';
    }
}