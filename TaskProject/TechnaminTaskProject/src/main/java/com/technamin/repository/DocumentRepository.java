package com.technamin.repository;


import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.result.UpdateResult;
import com.technamin.model.Document;
import com.technamin.service.MessagingService;
import com.technamin.service.impl.MessagingServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static com.technamin.constants.Messages.InfoMessages.UPDATE_RESULT_INFO;
import static com.technamin.constants.Messages.SuccessMessages.DOCUMENT_SUCCESSFULLY_SAVED;
import static com.technamin.constants.ProjectConstants.*;

public class DocumentRepository {
    private static final Logger LOGGER = LoggerFactory.getLogger(DocumentRepository.class);
    MessagingService messagingService = new MessagingServiceImpl();
    MongoClient mongoClient = new MongoClient(MONGO_DB_HOST, MONGO_DB_PORT);
    MongoDatabase database = mongoClient.getDatabase(MONGO_DB_DATABASE_NAME);
    MongoCollection<org.bson.Document> collection = database.getCollection(MONGO_DB_COLLECTION_NAME);

    public void saveDocuments(List<Document> documents) {
        documents.forEach(document -> {
            BasicDBObject searchQuery = new BasicDBObject("doc_id", document.getDocumentID());
            BasicDBObject updateFields = new BasicDBObject();
            updateFields.append("seq", document.getSequence());
            updateFields.append("data", document.getData());
            BasicDBObject setQuery = new BasicDBObject();
            setQuery.append("$set", updateFields);
            UpdateResult updateResult = collection.updateOne(searchQuery, setQuery, new UpdateOptions().upsert(true));
            LOGGER.info(DOCUMENT_SUCCESSFULLY_SAVED, document.getDocumentID());
            LOGGER.info(UPDATE_RESULT_INFO,updateResult);
            messagingService.sendMessages(document);
        });
    }
}