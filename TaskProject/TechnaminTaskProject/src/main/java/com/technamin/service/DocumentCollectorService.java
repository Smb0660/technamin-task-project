package com.technamin.service;

import com.technamin.model.Document;

import java.util.List;
import java.util.Map;

public interface DocumentCollectorService {
    Map<Long, List<Document>> convertDocumentListToMap();
}