package com.technamin.service;

import java.io.Reader;

public interface FileReaderService {
    Reader getReader(String path);
}