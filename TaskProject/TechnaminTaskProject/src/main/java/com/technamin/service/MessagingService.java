package com.technamin.service;

import com.technamin.model.Document;

public interface MessagingService {
    void sendMessages(Document document);
}