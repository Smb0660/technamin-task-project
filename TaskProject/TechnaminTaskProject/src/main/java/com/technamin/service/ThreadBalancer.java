package com.technamin.service;

import com.technamin.model.Document;

import java.util.List;
import java.util.Map;

public interface ThreadBalancer {
    void balanceThreads(List<Long> data, Map<Long, List<Document>> documents);
}