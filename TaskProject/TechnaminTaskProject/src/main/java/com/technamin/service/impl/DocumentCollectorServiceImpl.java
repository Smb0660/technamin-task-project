package com.technamin.service.impl;

import com.technamin.mapper.DocumentMapper;
import com.technamin.model.Document;
import com.technamin.service.DocumentCollectorService;
import com.technamin.service.FileReaderService;

import java.util.List;
import java.util.Map;

import static com.technamin.constants.ProjectConstants.DOCUMENT_FILE_PATH;
import static java.util.stream.Collectors.groupingBy;

public class DocumentCollectorServiceImpl implements DocumentCollectorService {

    FileReaderService readerService = new FileReaderServiceImpl();

    @Override
    public Map<Long, List<Document>> convertDocumentListToMap() {
        List<Document> documentList = DocumentMapper.documentJsonParser(readerService.getReader(DOCUMENT_FILE_PATH));
        return documentList.stream()
                .collect(groupingBy(Document::getDocumentID));
    }
}