package com.technamin.service.impl;

import com.technamin.service.FileReaderService;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;

import static com.technamin.constants.Messages.ErrorMessages.PATH_EXCEPTION;
import static com.technamin.constants.ProjectConstants.DOCUMENT_FILE_PATH;

public class FileReaderServiceImpl implements FileReaderService {

    public Reader getReader(String path) {
        try {
            return Files.newBufferedReader(Path.of(DOCUMENT_FILE_PATH));
        } catch (IOException e) {
            throw new InvalidPathException(path, PATH_EXCEPTION);
        }
    }
}