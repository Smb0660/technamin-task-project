package com.technamin.service.impl;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.tools.json.JSONWriter;
import com.technamin.model.Document;
import com.technamin.service.MessagingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.InvalidPathException;
import java.util.concurrent.TimeoutException;

import static com.technamin.constants.ProjectConstants.*;

public class MessagingServiceImpl implements MessagingService {
    ConnectionFactory connectionFactory = new ConnectionFactory();
    Connection connection;
    Channel channel;
    private static final Logger LOGGER = LoggerFactory.getLogger(MessagingServiceImpl.class);

    public void sendMessages(Document document) {
        configureConnectionFactory();
        try {
            connection = connectionFactory.newConnection();
            channel = connection.createChannel();
            channel.queueDeclare(RABBITMQ_QUEUE_NAME, false, false, false, null);
            JSONWriter rabbitmqJson = new JSONWriter();
            String jsonMessage = rabbitmqJson.write(document);
            channel.basicPublish("", RABBITMQ_QUEUE_NAME, null, jsonMessage.getBytes());
            LOGGER.info("Message published");
        } catch (IOException | TimeoutException e) {
            LOGGER.error(e.getCause().toString());
            LOGGER.info(e.getCause().toString());
            throw new RuntimeException(e);
        }
    }

    public void configureConnectionFactory() {
        connectionFactory.setHost(RABBITMQ_HOST);
        connectionFactory.setPort(RABBITMQ_PORT);
        connectionFactory.setUsername(RABBITMQ_USERNAME);
        connectionFactory.setPassword(RABBITMQ_PASSWORD);
        connectionFactory.setVirtualHost(RABBITMQ_VIRTUAL_HOST);
    }
}