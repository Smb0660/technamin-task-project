package com.technamin.service.impl;

import com.technamin.model.Document;
import com.technamin.repository.DocumentRepository;
import com.technamin.service.ThreadBalancer;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadBalancerImpl implements ThreadBalancer {
    DocumentRepository repository = new DocumentRepository();

    public void balanceThreads(List<Long> data, Map<Long, List<Document>> documents) {

        ExecutorService executorService = Executors.newFixedThreadPool(data.size());
        for (long key : data) {
            executorService.submit(() -> repository.saveDocuments(documents.get(key)));
        }
        executorService.shutdown();
    }
}