package com.technamin;

import com.technamin.service.ReceiverService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        try {
            Thread.sleep(21000);
            ReceiverService receiverService = new ReceiverService();
            receiverService.receiveMessages();
        } catch (InterruptedException e) {
            LOGGER.error(e.getCause().toString());
            throw new RuntimeException(e);
        }
    }
}