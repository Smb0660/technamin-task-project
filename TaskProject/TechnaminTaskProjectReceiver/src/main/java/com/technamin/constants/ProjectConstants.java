package com.technamin.constants;

public class ProjectConstants {

    private ProjectConstants() {
        throw new IllegalStateException();
    }

    public static final String RABBITMQ_QUEUE_NAME = "documents_queue";
    public static final String RABBITMQ_HOST = "rabbitmq";
    public static final String RABBITMQ_VIRTUAL_HOST = "/";
    public static final int RABBITMQ_PORT = 5672;
    public static final String RABBITMQ_USERNAME = "guest";
    public static final String RABBITMQ_PASSWORD = "guest";
}