package com.technamin.model;


import java.io.Serializable;

public class Document implements Serializable {

    private long documentID;

    private long sequence;

    private String data;

    private long time;

    public long getDocumentID(){
        return documentID;
    }

    public void setDocumentID(long documentID) {
        this.documentID = documentID;
    }

    public long getSequence() {
        return sequence;
    }

    public void setSequence(long sequence) {
        this.sequence = sequence;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Document{" +
                "documentID=" + documentID +
                ", sequence=" + sequence +
                ", data='" + data + '\'' +
                ", time=" + time +
                '}';
    }
}