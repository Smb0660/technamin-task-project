package com.technamin.service;

import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import com.technamin.model.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import static com.technamin.constants.ProjectConstants.*;
import static java.nio.charset.StandardCharsets.UTF_8;

public class ReceiverService {

    private final ConnectionFactory connectionFactory;
    private Connection connection;
    private Channel channel;
    private static final Logger LOGGER = LoggerFactory.getLogger(ReceiverService.class);

    public ReceiverService() {
        connectionFactory = new ConnectionFactory();
    }

    public void receiveMessages() {
        configureConnectionFactory();
        try {
            connection = connectionFactory.newConnection();
            channel = connection.createChannel();
            channel.queueDeclare(RABBITMQ_QUEUE_NAME, false, false, false, null);
            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                String message = new String(delivery.getBody(), UTF_8);
                Document document = new Gson().fromJson(message, Document.class);
                LOGGER.info("Document with update status received :{}", document);
            };
            channel.basicConsume(RABBITMQ_QUEUE_NAME, true, deliverCallback, consumerTag -> {
            });
        } catch (IOException e) {
            LOGGER.error(e.getCause().toString());
            LOGGER.info(e.getCause().toString());
            throw new RuntimeException(e);
        } catch (TimeoutException e) {
            LOGGER.error(e.getCause().toString());
            LOGGER.info(e.getCause().toString());
        }
    }

    public void configureConnectionFactory() {
        connectionFactory.setPort(RABBITMQ_PORT);
        connectionFactory.setHost(RABBITMQ_HOST);
        connectionFactory.setUsername(RABBITMQ_USERNAME);
        connectionFactory.setPassword(RABBITMQ_PASSWORD);
        connectionFactory.setVirtualHost(RABBITMQ_VIRTUAL_HOST);
    }
}